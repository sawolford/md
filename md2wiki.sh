#!/bin/sh
if [ $# -ne 1 ]; then
  echo Usage: $(basename $0) filename.md
  exit 1
fi
scrdir=$(dirname $(realpath $0))
ifile=$1
export TEXINPUTS="$scrdir:"
ofile=$(echo $ifile | cut -f1 -d.).wiki
pandoc \
  --standalone \
  --toc \
  --filter pandoc-xnos \
  --metadata-file=$scrdir/metadata.yaml \
  -f markdown \
  -t mediawiki $ifile \
  -o $ofile
