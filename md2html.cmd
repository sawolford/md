@echo off
if [%2]==[] goto err
set scrdir=%~dp0
set TEXINPUTS=%scrdir%
pandoc ^
  --standalone ^
  --toc ^
  --number-sections ^
  --mathjax ^
  --metadata title=%2 ^
  --highlight-style=%scrdir%tex.theme ^
  --include-in-header=%scrdir%pandoc.css ^
  --filter pandoc-xnos ^
  --metadata-file=%scrdir%metadata.yaml ^
  -f markdown ^
  -t html5+tex_math_dollars %~1 ^
  -o %~n1.html
goto end
:err
echo Usage: %0 filename.md title
:end
