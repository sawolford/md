#!/bin/sh
if [ $# -lt 1 -o $# -gt 2 ]; then
  echo Usage: $(basename $0) filename.md title
  exit 1
fi
scrdir=$(dirname $(realpath $0))
ifile=$1
export TEXINPUTS="$scrdir:"
base=$(echo $ifile | cut -f1 -d.)
ofile=$base.html
title="${2:-$base}"
pandoc \
  --standalone \
  --toc \
  --number-sections \
  --mathjax \
  --metadata title="$title" \
  --highlight-style=$scrdir/tex.theme \
  --include-in-header=$scrdir/pandoc.css \
  --filter pandoc-xnos \
  --metadata-file=$scrdir/metadata.yaml \
  -f markdown \
  -t html5+tex_math_dollars $ifile \
  -o $ofile
