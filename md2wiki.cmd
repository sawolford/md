@echo off
if [%1]==[] goto err
set scrdir=%~dp0
set TEXINPUTS=%scrdir%
pandoc ^
  --standalone ^
  --toc ^
  --filter pandoc-xnos ^
  --metadata-file=%scrdir%metadata.yaml ^
  -f markdown ^
  -t mediawiki %~1 ^
  -o %~n1.wiki
goto end
:err
echo Usage: %0 filename.md
:end
