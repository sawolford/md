@echo off
if [%1]==[] goto err
set scrdir=%~dp0
set TEXINPUTS=%scrdir%
mpm --list | findstr "sourcecodepro" >nul
if %ERRORLEVEL% EQU 0 (set monofont=Source Code Pro) else set monofont=inconsolata
pandoc ^
  --pdf-engine=xelatex ^
  --number-sections ^
  --include-in-header=%scrdir%markdown.tex ^
  --include-in-header=title.tex ^
  --highlight-style=%scrdir%tex.theme ^
  --variable monofont="%monofont%" ^
  -V geometry:margin=1in ^
  --filter pandoc-xnos ^
  --filter pandoc-latex-fontsize ^
  --metadata-file=%scrdir%metadata.yaml ^
  -f markdown-implicit_figures ^
  -t pdf %~1 ^
  -o %~n1.pdf
goto end
:err
echo Usage: %0 filename.md
:end
