#!/bin/sh
if [ $# -ne 1 ]; then
  echo Usage: $(basename $0) filename.md
  exit 1
fi
scrdir=$(dirname $(realpath $0))
ifile=$1
export TEXINPUTS="$scrdir:"
ofile=$(echo $ifile | cut -f1 -d.).pdf
pandoc \
  --pdf-engine=xelatex \
  --number-sections \
  --include-in-header=$scrdir/markdown.tex \
  --include-in-header=title.tex \
  --highlight-style=$scrdir/tex.theme \
  -V monofont:"Cascadia Code" \
  -V monofontoptions:"Scale=0.8" \
  -V geometry:margin=1in \
  --filter pandoc-xnos \
  --filter pandoc-latex-fontsize \
  --metadata-file=$scrdir/metadata.yaml \
  -f markdown-implicit_figures \
  -t pdf $ifile \
  -o $ofile
